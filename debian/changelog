hkl (5.1.3-1) UNRELEASED; urgency=medium

  [ Picca Frédéric-Emmanuel ]
  * d/gitlab-ci.yml: deleted, we use the default recipes

  [ Gilles Filippini ]
  * Support for HDF5 1.14.

  [ Roland Mas ]
  * Upload Gilles's patch to fix: "FTBFS against HDF5 1.14: Module
    ‘Bindings.HDF5.Raw’ does not export ‘H5L_info_t’", thanks to Gilles
    Filippini (Closes: #1087738).

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 5.1.3

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 15 Jan 2025 17:39:00 +0100

hkl (5.1.2-2) unstable; urgency=medium

  * fix i386 FTBFS by removing hardcoded -N4 ghc-options

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 16 Oct 2024 13:59:25 +0200

hkl (5.1.2-1) unstable; urgency=medium

  * New upstream version 5.1.2
  * d/control: Added B-D libghc-hspec-dev

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 16 Oct 2024 11:15:13 +0200

hkl (5.0.0.3462-1) unstable; urgency=medium

  * New upstream version 5.0.0.3462
  * Bug fix: "FTBFS: unrecognized option `-p&#39;", thanks to Santiago
    Vila (Closes: #1071315).
  * d/control: B-D added cabal-install.
  * d/control: B-D added gobject-introspection-bin | gobject-introspection.
    make the package backportable on bookworm.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sun, 02 Jun 2024 12:26:35 +0200

hkl (5.0.0.3434-1) unstable; urgency=medium

  [ Picca Frédéric-Emmanuel ]
  * Add missing build dependency on gobject-introspection-bin for addon gir.
  * New upstream version 5.0.0.3434
  * Bug fix: "hkl FTBFS with libcglm-dev 0.9.2-1", thanks to Adrian Bunk
    (Closes: #1063631).
  * d/control: Added B-D libinih-dev
  * d/libhkl5.symbols: Updated
  * d/rules: use the right syntax for dh_autoreconf

  [ Roland Mas ]
  * d/control: Added Myself as Uploaders.
  * d/control: B-D added libdatatype99-dev.
  * Bug fix: "Please remove dependency on install-info", thanks to
    hille42@web.de</a>; (Closes: #1013821).
  * Bug fix: "Fails to build source after successful build", thanks to
    Lucas Nussbaum (Closes: #1046274).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 19 Apr 2024 10:26:26 +0200

hkl (5.0.0.3381-1) unstable; urgency=medium

  * Added pristine-tar by default in d/gbp.conf
  * New upstream version 5.0.0.3381
  * d/control: set R-R-R: no

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 21 Nov 2023 15:19:35 +0100

hkl (5.0.0.3372-2) unstable; urgency=medium

  * Team upload
  * Drop unnecessary Build-Depends: libgtkglext1-dev

 -- Bastian Germann <bage@debian.org>  Wed, 01 Nov 2023 16:00:48 +0100

hkl (5.0.0.3372-1) unstable; urgency=medium

  * Set upstream metadata fields: Repository.
  * Update standards version to 4.6.2, no changes needed.
  * New upstream version 5.0.0.3372

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 29 Sep 2023 14:27:20 +0200

hkl (5.0.0.3357-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Added B-D libcglm-dev

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 23 Jun 2023 11:10:19 +0200

hkl (5.0.0.3001-1) unstable; urgency=medium

  * New upstream version 5.0.0.3001

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 24 May 2022 15:51:36 +0200

hkl (5.0.0.3000-1) unstable; urgency=medium

  * New upstream version 5.0.0.3000

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 20 May 2022 09:42:06 +0200

hkl (5.0.0.2994-1) unstable; urgency=medium

  * New upstream version 5.0.0.2994

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 13 May 2022 15:55:03 +0200

hkl (5.0.0.2960-1) unstable; urgency=medium

  * New upstream version 5.0.0.2959
  * New upstream version 5.0.0.2960
  * d/control: Added libhdf5-dev and libgl-dev to B-D.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 22 Mar 2022 18:50:04 +0100

hkl (5.0.0.2875-1) unstable; urgency=medium

  [ Neil Williams ]
  * Update control to add Debian PaN maintainers

  [ Picca Frédéric-Emmanuel ]
  * New upstream version 5.0.0.2875
  * Update standards version to 4.6.0, no changes needed.
  * wrap-and-sort -ast

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 08 Dec 2021 10:44:32 +0100

hkl (5.0.0.2816-2) unstable; urgency=medium

  * Bug fix: "binary-any FTBFS due to missing build dependency on
    gtk-doc-tools", thanks to Adrian Bunk (Closes: #994484).
    The autogens.sh scripts expect gtk-doc-tools.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 18 Sep 2021 10:41:33 +0200

hkl (5.0.0.2816-1) unstable; urgency=medium

  * New upstream version 5.0.0.2816
  * Bump debhelper from old 12 to 13.
  * Apply multi-arch hints. + libhkl-doc: Add Multi-Arch: foreign.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 16 Sep 2021 15:05:16 +0200

hkl (5.0.0.2661-1) unstable; urgency=medium

  * New upstream version 5.0.0.2661

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 02 Oct 2020 16:13:30 +0200

hkl (5.0.0.2658-1) unstable; urgency=medium

  * New upstream version 5.0.0.2658
  * d/control: put the doc in the Build-Depends-Indep (Closes: #845539)
  * d/control: use the right Homepage.
  * d/rules: do not create symbols for the binoculars library.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 25 Sep 2020 10:54:41 +0200

hkl (5.0.0.2650-1) unstable; urgency=medium

  * New upstream version 5.0.0.2650

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 21 Sep 2020 11:24:27 +0200

hkl (5.0.0.2620-1) unstable; urgency=medium

  * Added Autopkgtest.
  * d/control: Added libglib2-dev to libhkl-dev Depends.
  * d/rules: Removed --as-needed
  * Fix day-of-week for changelog entry 4.99.99.1946-1~exp1.
  * Update standards version to 4.5.0, no changes needed.
  * New upstream version 5.0.0.2620

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 01 May 2020 18:46:19 +0200

hkl (5.0.0.2616-1) unstable; urgency=medium

  * New upstream version 5.0.0.2616 (Closes: #947400)
    The fix was tested on an arm64 porter box.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 29 Apr 2020 21:20:00 +0200

hkl (5.0.0.2615-1) unstable; urgency=medium

  * New upstream version 5.0.0.2615
  * Bug fix: "hkl FTBFS on arm64: sirius segfaults", thanks to Adrian Bunk
    (Closes: #947400).
  * Bug fix: "FTBFS: ../../hkl/ccan/generator/generator.h:23:2: error:
    #error Generators require coroutines", thanks to Lucas Nussbaum
    (Closes: #929720).

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 29 Apr 2020 18:27:45 +0200

hkl (5.0.0.2569-1) unstable; urgency=medium

  * Use debhelper-compat instead of debian/compat.
  * New upstream version 5.0.0.2569
  * d/control:
    - Build-Depends: Added libgtk-3-dev.
    - Switched to Python3 (Closes: #943123)
    - Bump Standards-Version to 4.4.1 (nothing to do)
    - Switched to compat level 12.
    - Removed obsolete libhkl5-dbg package.
  * d/p/0001-889878-make-test-for-pointer-safe-makecontext-succee.patch:
  * d/rules:
    - removed dh_override_autoreconf
    - clean the sgml file (Closes: #939950)
    - Activated hardening=+all
    Removed since the real bug was fixed upstream.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 11 Dec 2019 18:18:21 +0100

hkl (5.0.0.2456-1) unstable; urgency=medium

  * New upstream version 5.0.0.2456
  * Use secure URI in Homepage field.
  * d/control
    - Build-Depends:
      - Added: elpa-htmlize thanks to Sean Whitton (Closes: #916867)
      - Droped: dh-autoconf.
    - Bump Standards-Version to 4.2.1 (nothing to do)
  * d/rules: run autogen.sh via dh_override_autoreconf
  * d/watch: Use the git mode
  * d/gitlab-ci.yml: Added for salsa CI.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 04 Feb 2019 15:31:54 +0100

hkl (5.0.0.2449-2) unstable; urgency=medium

  * fix for real (Closes: #889878) and a big thanks to
    Bernhard Übelacker for his investigations
  * d/patches
    - make-test-for-pointer-safe-makecontext-succeed (added)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 04 Aug 2018 10:42:33 +0200

hkl (5.0.0.2449-1) unstable; urgency=medium

  * New upstream version 5.0.0.2449 (Closes: #889878)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Thu, 08 Feb 2018 11:32:39 +0100

hkl (5.0.0.2447-1) unstable; urgency=medium

  * New upstream version 5.0.0.2447 (Closes: #868481)
  * d/control
    - Bump Standards-Version to 4.1.3 (extra -> optional)
    - Replaced emacs24 -> emacs (Closes: #870664)
    - Added gnuplot-nox, gnuplot-mode

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 07 Feb 2018 15:06:14 +0100

hkl (5.0.0.2173-2) unstable; urgency=medium

  * Build-Depends fixed for jessie-backports.
    (libgsl-dev | libgsl0-dev -> libgsl0-dev | libgsl-dev)

 -- Picca Frédéric-Emmanuel <picca@synchrotron-soleil.fr>  Mon, 31 Oct 2016 10:30:48 +0100

hkl (5.0.0.2173-1) unstable; urgency=medium

  * New upstream version 5.0.0.2173
  * debian/control
    - Bump Standards-Version to 3.9.6 (nothing to do)
    - Remove Multi-Arch same for libhkl-dev (Closes: #822840)
  * debian/rules
    - Use --with autoreconf (Closes: #727894)

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Mon, 19 Sep 2016 22:32:19 +0200

hkl (5.0.0.2080-1) unstable; urgency=medium

  * Imported Upstream version 5.0.0.2080

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 27 Apr 2016 09:23:31 +0200

hkl (4.99.99.1955-2) unstable; urgency=medium

  [ Andreas Beckmann ]
  * actually close the gsl-2 bug

  [ Picca Frédéric-Emmanuel ]
  * debian/control
    - Modify the Build-Depends in order to make the package backportable.
    - ligsl-dev -> libgsl-dev | libgsl0-dev

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 03 Feb 2016 16:09:02 +0100

hkl (4.99.99.1955-1.1) unstable; urgency=medium

  [ Bas Couwenberg ]
  * Non-maintainer upload.
  * Update build dependencies for GSL 2, change libgsl0-dev to libgsl-dev.
    (Closes: #807206)

 -- Andreas Beckmann <anbe@debian.org>  Mon, 18 Jan 2016 13:13:12 +0100

hkl (4.99.99.1955-1) unstable; urgency=medium

  * Imported Upstream version 4.99.99.1955

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Wed, 15 Jul 2015 16:09:46 +0200

hkl (4.99.99.1950-1) unstable; urgency=medium

  * Imported Upstream version 4.99.99.1950
  * Fix an FTBFS on sparc
  * Multi-arch also the gir package
  * Register documentation with doc-base
  * Add the symbols file for libhkl5

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 07 Jul 2015 13:29:18 +0200

hkl (4.99.99.1949-1~exp1) experimental; urgency=medium

  * Imported Upstream version 4.99.99.1949
  * debian/rules
    - Fix FTBFS on wheezy using MPLCONFIGDIR

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Fri, 03 Jul 2015 16:11:41 +0200

hkl (4.99.99.1946-1~exp1) experimental; urgency=low

  * Imported Upstream version 4.99.99.1946
  * debian/copyright updated
  * debian/control
    - Bump Standards-Version to 3.9.6 (nothing to do)
    - switch to compat level 9
    - multi-arch
    - New binary packages, libhkl5, gir1.2-hkl-5.0
    - Deleted binary package, libhkl4
    - Build-Depends:
      - added:
        + dh-autoreconf
        + emacs
        + gobject-introspection
        + gtk-doc-tools
        + libbullet-dev
        + libg3d-dev
        + libg3d-plugins,
        + libgirepository1.0-dev
        + libgtk2.0-dev
        + libgtkglext1-dev
        + libyaml-dev
        + python-gi
        + python-matplotlib
        + python-tk
      - deleted:
        - libgtkmm-1.2-dev
  * debian/rules
    - use dh_install --fail-missing to check for uninstalled files.

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 30 Jun 2015 10:22:27 +0200

hkl (4.0.3-4) unstable; urgency=low

  * debian/control
    - use the right debhelper version
  * debian/libhkl4.symbols
    - remove a symbol with-debian-revision

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Sat, 18 Jun 2011 17:26:18 +0200

hkl (4.0.3-3) unstable; urgency=low

  * debian/control
    - remove the Dm-Upload-Allowed (I am DD now)
    - Bump Standards-Version to 3.9.2 (nothing to do)
    - update the Vcs-* fields with the new anonscm
  * debian/copyright
    - use the dep5
    - update the copyright
  * update the patch information
  * fix two lintian warnings
  * add the symbols file

 -- Picca Frédéric-Emmanuel <picca@debian.org>  Tue, 14 Jun 2011 21:02:00 +0200

hkl (4.0.3-2) unstable; urgency=low

  * fix the watch file due to an upgrade of the upstream website
  * fix the FTBS on mips by reducing the number of bench test
  * upgrade to 3.9.0 policy (nothing to do)
  * use the right homepage
  * fix the libhkl4 section.

 -- Picca Frédéric-Emmanuel <picca@synchrotron-soleil.fr>  Mon, 05 Jul 2010 14:19:53 +0200

hkl (4.0.3-1) unstable; urgency=low

  * Initial release. (Closes: #569153)
  * Thanks to Justin B Rye for package description review

 -- Picca Frédéric-Emmanuel <picca@synchrotron-soleil.fr>  Sun, 07 Feb 2010 12:39:15 +0100
