[dispatcher]
#:
#: ncores:
#:
#: the number of cores use for computation.
#: the effective number of core used depends on the real number of core available on your computer.
#: at maximum this value could be one less that the pysical number of core.
#:
#: default value: `4`
#:
#: uncomment and edit the next line if you want to modify the value
ncores: 4
#:
#: destination:
#:
#: the template used to produce the destination file
#:
#: You can write what you want in the field but it need to end with .h5
#: The generated file is an hdf5 file. You can add in the text some specific
#: string which will be replace with values extracted from the configuration
#: when saving the file.
#:
#: These parameters are:
#:   `{first}`      - replaced by the first scan numer of the serie
#:   `{last}`       - replaced by the last scan numer of the serie
#:   `{limits}`     - replaced by the limits of the final cube or nolimits
#:   `{projection}` - the projection name
#:
#: default value: `{projection}_{first}-{last}_{limits}.h5`
#:
#: uncomment and edit the next line if you want to modify the value
destination: {projection}_{first}-{last}_{limits}.hdf5
#:
#: overwrite:
#:
#:  `true` - the output file name is always the same, so it is overwriten when
#: recomputed with the same parameter.
#:  `false` - the output is modifier and `_<number>` is added to the filename in order
#:  to avoid overwriting them.
#:
#: default value: `false`
#:
#: uncomment and edit the next line if you want to modify the value
overwrite: false
[input]
#:
#: type:
#:
#: Define the experimental setup and the type of scan used to acquire the data
#:
#: the list of the available values are:
#:
#:  - cristal:k6c
#:  - mars:flyscan
#:  - mars:sbs
#:  - sixs:flymedh
#:  - sixs:flymedhgisaxs
#:  - sixs:flymedv
#:  - sixs:flymedvgisaxs
#:  - sixs:flyuhv
#:  - sixs:flyuhvgisaxs
#:  - sixs:sbsmedh
#:  - sixs:sbsmedhgisaxs
#:  - sixs:sbsmedv
#:  - sixs:sbsmedvgisaxs
#:  - sixs:sbsuhv
#:  - sixs:sbsuhvgisaxs
#:
#: `sbs` means step by step scan.
#:
#: `fly` or `flyscan` means flyscan.
#:
#: `medh` or `medv` or `uhv` refers to the diffractometer.
#:
#: Some configurations specify the detector like eiger/s70...
#: look at the `detector` parameter if you need to select another one.
#:
#: This parameter defines how to find the datas in the hdf5 file.
#:
#: default value: `sixs:flyuhv`
#:
#: uncomment and edit the next line if you want to modify the value
type: diffabs:cirpad
#:
#: nexusdir:
#:
#: Directory path where binoculars-ng looks for the data.
#:
#: All sub directories are explored.
#: It can be relative to the current directory or absolute
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
nexusdir: /nfs/ruche-diffabs/diffabs-soleil/com-diffabs/2024/Run3/2024-05-28/
#:
#: inputtmpl:
#:
#: data files are matched with this template in order to decide if it must be used for computation.
#:
#: the template is pre-processed with the printf logic and an integer value comming from the the `inputrange`
#: for example with the default value `%05d` and this `inputrange=1-10`, all supported files which contains
#: 00001, 00002, ... 00010 are used during the computation.
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
inputtmpl: %04d
#:
#: inputrange:
#:
#: Indexes of the scans you want to process
#:
#: There are two ways to enter these values.
#:   `n`         - a single scan with index n
#:   `n-m`       - a range of scans from n to m (included)
#:
#:   `n-m,p,...` - combination of scans indexes separated by a coma (no space allowed).
#:
#: default value: `1`
#:
#: uncomment and edit the next line if you want to modify the value
inputrange: 1
#:
#: detector:
#:
#: The detector name
#:
#: In order to process the data, we need the 3D position of each pixel
#: in the laboratory basis. Since every detector has it's own geometry,
#: it is necessary to provide this information unambiguously.
#:
#: Some data files contain more than one detectors, so this parameter allows
#: to select the right one.
#:
#: the list of known detectors is:
#:  - ImXpadS140
#:  - XpadFlatCorrected
#:  - ImXpadS70
#:  - Eiger1M
#:  - Ufxc
#:  - Merlin
#:  - MerlinMedipix3RXQuad
#:  - MerlinMedipix3RXQuad512
#:
#: default value: `ImXpadS140`
#:
#: uncomment and edit the next line if you want to modify the value
detector: Cirpad
#:
#: centralpixel:
#:
#: x,y coordinates in pixels of the direct beam on the detector.
#:
#: with `sdd` and `detrot` this parameter allow to set the
#: detector position on the detector's arm when all motors
#: are set equal to `0`.
#:
#: default value: `0,0`
#:
#: uncomment and edit the next line if you want to modify the value
centralpixel: 0,0
#:
#: sdd:
#:
#: sample-detector-distance expressed in meter.
#:
#: with `centralpixel` and `detrot` this parameter allow to set the
#: detector position on the detector's arm when all motors
#: are set equal to `0`.
#:
#: default value: `1.0`
#:
#: uncomment and edit the next line if you want to modify the value
sdd: 1.0
#:
#: detrot:
#:
#: rotation of the detector along the x axis expressed in Degrees.
#:
#: with `centralpixel` and `sdd` this parameter allow to set the
#: detector position on the detector's arm when all motors
#: are set equal to `0`.
#:
#: default value: `0.0`
#:
#: uncomment and edit the next line if you want to modify the value
detrot: 0.0
#:
#: attenuation_coefficient:
#:
#: the attenuation coefficient used to correct the detector's data.
#:
#:  `<not set>` - no correction is applyed
#:  `value`     - a correction is applyed
#:
#: The `value` depends on the energy of the experiment.
#:
#: On Sixs there is two different kind of attenuation system.
#:  `new attenuation` - based on piezo actuators (fast)
#:  `old attenuation` - based on pneumatic` actuators (slower)
#:
#: both system save the same kind of data's in the data files.
#: An attenuation value `v` at each step of the scan which allows
#: to compute the image correction with this formula:
#:
#:   `attenuation_coef ** v`
#:
#: The only difference is a shift of the index for the the new attenuation.
#: The applyed correction for the n-th point is computed with the `value`
#: at n + 2
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# attenuation_coefficient:
#:
#: attenuation_max:
#:
#: maximum attenuation allow for the attenuation correction.
#:
#:  `<not set>` - always apply the attenuation correction.
#:  `max`       - apply the attenuation if `v` is lower or equal to `max`
#:
#: `v` is the value stored by the attenuation system in the data file.
#:
#: This parameter has an effect only if the `attenuation_coefficient`
#: was previously set.
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# attenuation_max:
#:
#: attenuation_shift:
#:
#: shift the index of the attenuation.
#:
#:  `<not set>` - use the default offset. (0 or 2 depending of the kind of scan)
#:  `shift`     - force the shift to the given value.
#:
#: This `shift` correspond to a decalage between the attenuator values and the images.
#: the attenuation corresponding to the image at the position `n` in a scan
#: is in fact `n + shift`
#:
#: This parameter has an effect only if the `attenuation_coefficient`
#: was previously set.
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# attenuation_shift:
#:
#: maskmatrix:
#:
#: name of the file which contain the detector mask or `default`
#:
#: The supported type of file is for now only .npy
#:
#: the `default` value can be set in order to use the default
#: mask of the detector. Some of them have by default some area
#: which required to be masked (pixels with non standard surface,
#: area with holes).
#:
#: Most of the time a mask file was generated during ther experiment.
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# maskmatrix:
#:
#: wavelength:
#:
#: overwrite the wavelength from the data file with the one provided.
#:
#:  `<not set>`  - use the data file wavelength
#:  `wavelength` - overwrite the data file value with this one.
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# wavelength:
#:
#: image_sum_max:
#:
#: maximum intensity value allow for the sum of all pixels in an image in order to process it.
#:
#:  `<not set>` - process all images.
#:  `max`       - process images only if the sum of all the pixels is lower than `max`
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# image_sum_max:
#:
#: skip_first_points:
#:
#: skip the first `n` points of each scan.
#:
#:  `<not set>` - process all images.
#:  `n`         - skip the first `n` points (`n` included)
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# skip_first_points:
#:
#: skip_last_points:
#:
#: skip the last `n` points of each scan.
#:
#:  `<not set>` - process all images.
#:  `n`         - skip the last `n` points (`n` included)
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# skip_last_points:
#:
#: polarization_correction:
#:
#:  `true` - apply the polarization correctionthe to the intensities.
#:  `false` - do not apply the polarization correction.
#:  to avoid overwriting them.
#:
#: default value: `false`
#:
#: uncomment and edit the next line if you want to modify the value
polarization_correction: false
#:
#: surface_orientation:
#:
#: The orientation of the surface.
#:
#: the list of the available orientation are:
#:
#:  - vertical
#:  - horizontal
#:
#: this orientation if for all the Geometry axes set to zero and correspond to
#: the orientation of a vector collinear to the surface.
#:
#: default value: `vertical`
#:
#: uncomment and edit the next line if you want to modify the value
surface_orientation: vertical
#:
#: datapath:
#:
#: `datapath` internal value used to find the data in the data file.
#:
#: This value is for expert only.
#:
#: default value: <not set>
#:
#: default value: `[{"attenuationPath":{"contents":{"contents":[0,{"contents":"scan_data/attenuation","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"attenuationPathCoefficient":0,"attenuationPathMax":null,"attenuationPathOffset":2,"tag":"DataSourcePath'Attenuation"},{"geometryGeometry":{"contents":["Uhv",null],"tag":"Geometry'Factory"},"geometryPathAxes":[{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_MU","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_OMEGA","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_DELTA","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_GAMMA","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"}],"geometryPathWavelength":{"contents":{"contents":{"contents":[0,{"contents":"SIXS/Monochromator/wavelength","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},"tag":"DataSourcePath'Geometry"},[{"contents":{"contents":[0,{"contents":"scan_data/xpad_image","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},{"detector":"ImXpadS140"}],{"contents":{"contents":{"contents":[0,{"contents":"scan_data/epoch","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Timestamp"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/epoch","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Timescan0"}]`
#:
#: uncomment and edit the next line if you want to modify the value
datapath: [{"attenuationPath":{"contents":{"contents":[0,{"contents":"scan_data/attenuation","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"attenuationPathCoefficient":0,"attenuationPathMax":null,"attenuationPathOffset":2,"tag":"DataSourcePath'Attenuation"},{"geometryGeometry":{"contents":["Uhv",null],"tag":"Geometry'Factory"},"geometryPathAxes":[{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_MU","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_OMEGA","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_DELTA","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/UHV_GAMMA","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"}],"geometryPathWavelength":{"contents":{"contents":{"contents":[0,{"contents":"SIXS/Monochromator/wavelength","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Double"},"tag":"DataSourcePath'Geometry"},[{"contents":{"contents":[0,{"contents":"scan_data/xpad_image","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},{"detector":"ImXpadS140"}],{"contents":{"contents":{"contents":[0,{"contents":"scan_data/epoch","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Timestamp"},{"contents":{"contents":{"contents":[0,{"contents":"scan_data/epoch","tag":"H5DatasetPath"}],"tag":"H5GroupAtPath"},"tag":"H5RootPath"},"tag":"DataSourcePath'Timescan0"}]
[projection]
#:
#: type:
#:
#: The type of projection that can be computed with binoculars-ng
#:
#: the list of the available projections are:
#:
#:  - angles
#:  - angles2
#:  - hkl
#:  - qcustom
#:  - qindex
#:  - qparqper
#:  - qxqyqz
#:  - realspace
#:  - pixels
#:  - test
#:
#: Some projections can be customize using the `subprojection` parameter.
#:
#: default value: `qcustom`
#:
#: uncomment and edit the next line if you want to modify the value
type: qcustom
#:
#: resolution:
#:
#: The resolution of the bins expected for the projection's axes
#:
#: The expected value are:
#:   - one double - same resolution for all axes.
#:   - one double per axis - each axis has it's own resolution.
#:
#: the latter form use a comma to separate the values and no space is allowed.
#:
#: default value: `1.0e-2,1.0e-2,1.0e-2`
#:
#: uncomment and edit the next line if you want to modify the value
resolution: 1.0e-2,1.0e-2,1.0e-2
#:
#: limits:
#:
#: The limits of the bins expected for the projection's axes
#:
#: Sometime it is interesting to focus on a specific region of a map.
#: this allows to increase the resolution and keep a memory footprint acceptable.
#:
#: The expected value is a list of <limits>. One per axis.
#:
#:   `[<limits>,<limits>,<limits>]`
#:
#: <limits> has this form `<double or nothing>:<double or nothing>`.
#: nothing means realy nothing and in this case their is no limits.
#:
#: example:
#:   - [:1,2:3,4:5]
#:   - [:,2:3,4:]
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# limits:
#:
#: subprojection:
#:
#: The sub-projection that can be computed with binoculars-ng
#:
#: the list of the available sub-projections are:
#:
#:  - qx_qy_qz
#:  - q_tth_timestamp
#:  - q_timestamp
#:  - qpar_qper_timestamp
#:  - qpar_qper
#:  - q_phi_qx
#:  - q_phi_qy
#:  - q_phi_qz
#:  - q_stereo
#:  - deltalab_gammalab_sampleaxis
#:  - x_y_z
#:  - y_z_timestamp
#:  - q_qpar_qper
#:  - qpars_qper_timestamp
#:  - qpar_qper_sampleaxis
#:  - q_sampleaxis_tth
#:  - q_sampleaxis_timestamp
#:  - qx_qy_timestamp
#:  - qx_qz_timestamp
#:  - qy_qz_timestamp
#:  - tth_azimuth
#:  - q_timescan0
#:
#: default value: `qx_qy_qz`
#:
#: uncomment and edit the next line if you want to modify the value
subprojection: qx_qy_qz
#:
#: uqx:
#:
#: rotation around the x-axis of the sample in the surface basis system -- degree
#:
#: in this basis, the x-axis is colinear to the surface of the sample along the x-rays.
#:
#:  `<not set>` - use the default value `0.0`
#:  `a value`   - use this value
#:
#: default value: `0.0`
#:
#: uncomment and edit the next line if you want to modify the value
uqx: 0.0
#:
#: uqy:
#:
#: rotation around the y-axis of the sample in the surface basis system -- degree
#:
#: in this basis, the y-axis is colinear to the surface of the sample and
#: forme a directe basis with x-axis and z-axis.
#:
#: examples:
#:   - all motors set to zero and a vertical surface - y-axis along -z (labo basis)
#:   - all motors set to zero and an horizontal surcafe - y-axis along y (labo basis)
#:
#:  `<not set>` - use the default value `0.0`
#:  `a value`   - use this value
#:
#: default value: `0.0`
#:
#: uncomment and edit the next line if you want to modify the value
uqy: 0.0
#:
#: uqz:
#:
#: rotation around the z-axis of the sample in the surface basis system -- degree
#:
#: in this basis, the z-axis is perpendicular to the surface of the sample.
#:
#: examples:
#:   - all motors set to zero and a vertical surface - z-axis along y (labo basis)
#:   - all motors set to zero and an horizontal surcafe - z-axis along z (labo basis)
#:
#:
#:  `<not set>` - use the default value `0.0`
#:  `a value`   - use this value
#:
#: default value: `0.0`
#:
#: uncomment and edit the next line if you want to modify the value
uqz: 0.0
#:
#: sampleaxis:
#:
#: the name of the sample axis expected by some subprojections.
#:
#:  `<not set>` - for all subprojections which does not expect a value.
#:  `a value`   - use this value for the subprojection expecting this axis.
#:
#: default value: `<not set>`
#:
#: uncomment and edit the next line if you want to modify the value
# sampleaxis:
